using AttendancePlus.AdminApi.Data;
using AttendancePlus.AdminApi.Data.Core;
using AttendancePlus.AdminApi.Data.Model;
using AttendancePlus.AdminApi.Grpc;
using FluentValidation;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;
namespace AttendancePlus.AdminApi.Services;

public class SchoolAdministrationGrpcService : SchoolAdministrationService.SchoolAdministrationServiceBase {
    private readonly ILogger<SchoolAdministrationGrpcService> _logger;
    private readonly IUnitOfWork<AdministrationDbContext> _unitOfWork;    
    private readonly IRepository<User, AdministrationDbContext, string> _userRepository;
    private readonly IRepository<School, AdministrationDbContext, string> _schoolRepository;

    
    private readonly IValidator<AddSchoolRequest> _addSchoolRequestValidator;
    private readonly IValidator<DeleteSchoolRequest> _deleteSchoolRequestValidator;
    private readonly IValidator<GetSchoolRequest> _getSchoolRequestValidator;
    private readonly IValidator<UpdateSchoolRequest> _updateSchoolRequestValidator;
    private readonly IValidator<GetSchoolsRequest> _getSchoolsRequestValidator;
    private readonly IValidator<AddSchoolAdminRequest> _addSchoolAdminRequestValidator;
    private readonly IValidator<UpdateSchoolAdminRequest> _updateSchoolAdminRequestValidator;
    
    public SchoolAdministrationGrpcService(ILogger<SchoolAdministrationGrpcService> logger, IUnitOfWork<AdministrationDbContext> unitOfWork, IValidator<AddSchoolRequest> addSchoolRequestValidator, IValidator<DeleteSchoolRequest> deleteSchoolRequestValidator, IValidator<GetSchoolRequest> getSchoolRequestValidator, IValidator<UpdateSchoolRequest> updateSchoolRequestValidator, IValidator<GetSchoolsRequest> getSchoolsRequestValidator, IValidator<AddSchoolAdminRequest> addSchoolAdminRequestValidator, IValidator<UpdateSchoolAdminRequest> updateSchoolAdminRequestValidator, IRepository<User, AdministrationDbContext, string> userRepository, IRepository<School, AdministrationDbContext, string> schoolRepository) {
        _logger = logger;
        _unitOfWork = unitOfWork;
        _addSchoolRequestValidator = addSchoolRequestValidator;
        _deleteSchoolRequestValidator = deleteSchoolRequestValidator;
        _getSchoolRequestValidator = getSchoolRequestValidator;
        _updateSchoolRequestValidator = updateSchoolRequestValidator;
        _getSchoolsRequestValidator = getSchoolsRequestValidator;
        _addSchoolAdminRequestValidator = addSchoolAdminRequestValidator;
        _updateSchoolAdminRequestValidator = updateSchoolAdminRequestValidator;
        _userRepository = userRepository;
        _schoolRepository = schoolRepository;
    }

    public override Task<AddSchoolResponse> AddSchool(AddSchoolRequest request, ServerCallContext context) {
        return base.AddSchool(request, context);
    }

    public override Task<DeleteSchoolResponse> DeleteSchool(DeleteSchoolRequest request, ServerCallContext context) {
        return base.DeleteSchool(request, context);
    }

    public override Task<GetSchoolResponse> GetSchool(GetSchoolRequest request, ServerCallContext context) {
        return base.GetSchool(request, context);
    }

    public override Task<UpdateSchoolResponse> UpdateSchool(UpdateSchoolRequest request, ServerCallContext context) {
        return base.UpdateSchool(request, context);
    }

    public override Task<GetSchoolsResponse> GetSchools(GetSchoolsRequest request, ServerCallContext context) {
        return base.GetSchools(request, context);
    }

    public override Task<AddSchoolAdminResponse> AddSchoolAdmin(AddSchoolAdminRequest request,
        ServerCallContext context) {
        return base.AddSchoolAdmin(request, context);
    }

    public override Task<UpdateSchoolAdminResponse> UpdateSchoolAdmin(UpdateSchoolAdminRequest request,
        ServerCallContext context) {
        return base.UpdateSchoolAdmin(request, context);
    }
}