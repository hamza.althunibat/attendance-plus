using AttendancePlus.AdminApi.Data;
using AttendancePlus.AdminApi.Data.Core;
using AttendancePlus.AdminApi.Data.Model;
using AttendancePlus.AdminApi.Grpc;
using FluentValidation;
using Grpc.Core;
using NodaTime;
using NodaTime.Extensions;

namespace AttendancePlus.AdminApi.Services;

public class AdministrationGrpcService : AdministrationService.AdministrationServiceBase {
    private readonly IValidator<AddSuperAdminRequest> _addSuperAdminValidator;
    private readonly IValidator<DeleteUserRequest> _deleteUserValidator;
    private readonly IValidator<GetUserRequest> _getUserValidator;
    
    private readonly ILogger<AdministrationGrpcService> _logger;
    private readonly IUnitOfWork<AdministrationDbContext> _unitOfWork;
    private readonly IRepository<User, AdministrationDbContext, string> _userRepository;

    public AdministrationGrpcService(IRepository<User, AdministrationDbContext, string> userRepository,
        ILogger<AdministrationGrpcService> logger, IValidator<DeleteUserRequest> deleteUserValidator,
        IUnitOfWork<AdministrationDbContext> unitOfWork, IValidator<GetUserRequest> getUserValidator,
        IValidator<AddSuperAdminRequest> addSuperAdminValidator) {
        _userRepository = userRepository;
        _logger = logger;
        _deleteUserValidator = deleteUserValidator;
        _unitOfWork = unitOfWork;
        _getUserValidator = getUserValidator;
        _addSuperAdminValidator = addSuperAdminValidator;
    }

    public override async Task<DeleteUserResponse> DeleteUser(DeleteUserRequest request, ServerCallContext context) {
        _logger.LogInformation("Call Started {Method}", nameof(DeleteUser));
        try {
            var validation = _deleteUserValidator.Validate(request);
            if (!validation.IsValid)
                return new DeleteUserResponse {
                    Success = false,
                    Error = $"Validation Error Occur: {string.Join("|", validation.Errors.Select(e => e.ErrorMessage))}"
                };

            var deleted = await _userRepository.Delete(request.UserId).ConfigureAwait(false);
            if (!deleted)
                return new DeleteUserResponse {
                    Success = false,
                    Error = $"Validation Error Occur, user with id {request.UserId} was not found!"
                };
            await _userRepository.Delete(request.UserId);
            var result = await _unitOfWork.SaveAsync();
            if (result.Succeeded)
                return new DeleteUserResponse { Success = true };
            return new DeleteUserResponse {
                Success = false,
                Error = $"Unable to process request!, an error occur: {result.Error}"
            };
        }
        catch (Exception e) {
            _logger.LogError(e, "Unable to process request!");
            return new DeleteUserResponse {
                Error = $"Unable to process request!, an error occur: {e.Message}",
                Success = false
            };
        }
    }

    public override async Task<GetUserResponse> GetUser(GetUserRequest request, ServerCallContext context) {
        _logger.LogInformation("Call Started {Method}", nameof(GetUser));
        try {
            var validation = _getUserValidator.Validate(request);
            if (!validation.IsValid)
                return new GetUserResponse {
                    Success = false,
                    Error = $"Validation Error Occur: {string.Join("|", validation.Errors.Select(e => e.ErrorMessage))}"
                };

            var user = await _userRepository.GetFirstOrDefaultBy(x => x.Id == request.UserId, x => x)
                .ConfigureAwait(false);
            if (user != null)
                return user.ToResponse();
            return new GetUserResponse {
                Success = false,
                Error = $"Record was not Found, using Id: {request.UserId}"
            };
        }
        catch (Exception e) {
            _logger.LogError(e, "Unable to process request!");
            return new GetUserResponse {
                Error = $"Unable to process request!, an error occur: {e.Message}",
                Success = false
            };
        }
    }

    public override async Task<AddSuperAdminResponse> AddSuperAdmin(AddSuperAdminRequest request,
        ServerCallContext context) {
        _logger.LogInformation("Call Started {Method}", nameof(AddSuperAdmin));
        try {
            var validation = _addSuperAdminValidator.Validate(request);
            if (!validation.IsValid)
                return new AddSuperAdminResponse {
                    Success = false,
                    Error = $"Validation Error Occur: {string.Join("|", validation.Errors.Select(e => e.ErrorMessage))}"
                };

            var emailIsAlreadyUsed =
                await _userRepository.Any(x => x.Email == request.Email.ToLower()).ConfigureAwait(false);

            if (emailIsAlreadyUsed)
                return new AddSuperAdminResponse {
                    Success = false,
                    Error = $"Validation Error Occur: email {request.Email} is already being used!"
                };
            var now = ZonedDateTime.FromDateTimeOffset(DateTimeOffset.Now);

            var user = new User(Helpers.GenerateId()) {
                CreatedOn = now,
                Names = request.Names.FromDto(),
                Email = request.Email.ToLower(),
                UserKey = Helpers.RandomTokenString(),
                Role = Role.SuperAdmin
            };
            await _userRepository.Add(user);
            var result = await _unitOfWork.SaveAsync();
            if (result.Succeeded)
                return new AddSuperAdminResponse { Success = true, UserId = user.Id };
            return new AddSuperAdminResponse {
                Success = false,
                Error = $"Unable to process request!, an error occur: {result.Error}"
            };
        }
        catch (Exception e) {
            _logger.LogError(e, "Unable to process request!");
            return new AddSuperAdminResponse {
                Error = $"Unable to process request!, an error occur: {e.Message}",
                Success = false
            };
        }
    }

    public override Task<UpdateUserEmailResponse> UpdateUserEmail(UpdateUserEmailRequest request,
        ServerCallContext context) {
        return base.UpdateUserEmail(request, context);
    }

    public override Task<UpdateUserMobileNumberResponse> UpdateUserMobileNumber(UpdateUserMobileNumberRequest request,
        ServerCallContext context) {
        return base.UpdateUserMobileNumber(request, context);
    }
}