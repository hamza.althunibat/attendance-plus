using AttendancePlus.AdminApi.Data.Model;
using AttendancePlus.AdminApi.Grpc;
using Google.Protobuf.WellKnownTypes;

namespace AttendancePlus.AdminApi.Services;

public static class Extensions {
    public static NameDto ToDto(this Name name) {
        var (language, text) = name;
        return new NameDto {
            Language = language,
            Text = text
        };
    }

    public static UserClaimDto ToDto(this UserClaim claim) {
        return new UserClaimDto {
            Id = claim.Id,
            Name = claim.Name,
            Value = claim.Value
        };
    }

    public static Name FromDto(this NameDto name) {
        return new Name(name.Language, name.Text);
    }

    public static ISet<Name> FromDto(this IEnumerable<NameDto> names) {
        return names.Select(n => n.FromDto()).ToHashSet();
    }

    public static SchoolDto ToDto(this School school) {
        return new SchoolDto {
            Id = school.Id,
            Names = { school.Names.Select(n => n.ToDto()) }
        };
    }

    public static RoleDto ToDto(this Role role) {
        return (RoleDto)(int)role;
    }

    public static UserDto ToDto(this User user) {
        return new UserDto {
            Id = user.Id,
            Names = { user.Names.Select(n => n.ToDto()) },
            Email = user.Email,
            Role = user.Role.ToDto(),
            EmailConfirmed = user.EmailConfirmed.HasValue
                ? Timestamp.FromDateTimeOffset(user.EmailConfirmed.Value)
                : null,
            MobileNumber = user.MobileNumber,
            MobileNumberConfirmed = user.MobileNumberConfirmed.HasValue
                ? Timestamp.FromDateTimeOffset(user.MobileNumberConfirmed.Value)
                : null,
            Claims = { user.Claims.Select(c => c.ToDto()) }
        };
    }

    public static GetSchoolResponse ToResponse(this School school) {
        return new GetSchoolResponse {
            Success = school != null,
            Error = school == null ? "No Records Found" : null,
            School = school != null ? school.ToDto() : null
        };
    }

    public static GetUserResponse ToResponse(this User user) {
        return new GetUserResponse {
            Success = user != null,
            Error = user == null ? "No Records Found" : null,
            User = user != null ? user.ToDto() : null
        };
    }
}