﻿using System;
using System.Collections.Generic;
using AttendancePlus.AdminApi.Data.Model;
using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;
using NodaTime;

#nullable disable

namespace AttendancePlus.AdminApi.Migrations
{
    public partial class UpdatedScheme : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:relation", "father,mother,others")
                .Annotation("Npgsql:Enum:role", "school_admin,campus_admin,homeroom_supervisor,bus_supervisor,bus_device,homeroom_device,parent,user,super_admin")
                .Annotation("Npgsql:PostgresExtension:postgis", ",,");

            migrationBuilder.CreateTable(
                name: "School",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    Names = table.Column<ISet<Name>>(type: "jsonb", nullable: false),
                    xmin = table.Column<uint>(type: "xid", rowVersion: true, nullable: false),
                    CreatedOn = table.Column<ZonedDateTime>(type: "timestamp with time zone", nullable: false),
                    UpdatedOn = table.Column<ZonedDateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_School", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    MobileNumber = table.Column<string>(type: "character varying(15)", maxLength: 15, nullable: true),
                    Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    MobileNumberConfirmed = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    EmailConfirmed = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    Names = table.Column<ISet<Name>>(type: "jsonb", nullable: false),
                    UserKey = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    Role = table.Column<Role>(type: "role", nullable: false),
                    xmin = table.Column<uint>(type: "xid", rowVersion: true, nullable: false),
                    CreatedOn = table.Column<ZonedDateTime>(type: "timestamp with time zone", nullable: false),
                    UpdatedOn = table.Column<ZonedDateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Campus",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    Names = table.Column<ISet<Name>>(type: "jsonb", nullable: false),
                    SchoolId = table.Column<string>(type: "character varying(32)", nullable: false),
                    Location = table.Column<Point>(type: "geometry (point)", nullable: false),
                    xmin = table.Column<uint>(type: "xid", rowVersion: true, nullable: false),
                    CreatedOn = table.Column<ZonedDateTime>(type: "timestamp with time zone", nullable: false),
                    UpdatedOn = table.Column<ZonedDateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Campus_School_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "School",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserClaim",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    UserId = table.Column<string>(type: "character varying(32)", nullable: false),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Value = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    xmin = table.Column<uint>(type: "xid", rowVersion: true, nullable: false),
                    CreatedOn = table.Column<ZonedDateTime>(type: "timestamp with time zone", nullable: false),
                    UpdatedOn = table.Column<ZonedDateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserClaim_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Bus",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    CampusId = table.Column<string>(type: "character varying(32)", nullable: false),
                    Names = table.Column<ISet<Name>>(type: "jsonb", nullable: false),
                    xmin = table.Column<uint>(type: "xid", rowVersion: true, nullable: false),
                    CreatedOn = table.Column<ZonedDateTime>(type: "timestamp with time zone", nullable: false),
                    UpdatedOn = table.Column<ZonedDateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bus_Campus_CampusId",
                        column: x => x.CampusId,
                        principalTable: "Campus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    Location = table.Column<Point>(type: "geometry (point)", nullable: false),
                    BusId = table.Column<string>(type: "character varying(32)", nullable: false),
                    Names = table.Column<ISet<Name>>(type: "jsonb", nullable: false),
                    Parents = table.Column<ISet<ParentDetails>>(type: "jsonb", nullable: false),
                    xmin = table.Column<uint>(type: "xid", rowVersion: true, nullable: false),
                    CreatedOn = table.Column<ZonedDateTime>(type: "timestamp with time zone", nullable: false),
                    UpdatedOn = table.Column<ZonedDateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Student_Bus_BusId",
                        column: x => x.BusId,
                        principalTable: "Bus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Bus_CampusId",
                table: "Bus",
                column: "CampusId");

            migrationBuilder.CreateIndex(
                name: "IX_Bus_Names",
                table: "Bus",
                column: "Names")
                .Annotation("Npgsql:IndexMethod", "gin")
                .Annotation("Npgsql:IndexOperators", new[] { "jsonb_ops" });

            migrationBuilder.CreateIndex(
                name: "IX_Campus_Names",
                table: "Campus",
                column: "Names")
                .Annotation("Npgsql:IndexMethod", "gin")
                .Annotation("Npgsql:IndexOperators", new[] { "jsonb_ops" });

            migrationBuilder.CreateIndex(
                name: "IX_Campus_SchoolId",
                table: "Campus",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_School_Names",
                table: "School",
                column: "Names")
                .Annotation("Npgsql:IndexMethod", "gin")
                .Annotation("Npgsql:IndexOperators", new[] { "jsonb_ops" });

            migrationBuilder.CreateIndex(
                name: "IX_Student_BusId",
                table: "Student",
                column: "BusId");

            migrationBuilder.CreateIndex(
                name: "IX_Student_Names",
                table: "Student",
                column: "Names")
                .Annotation("Npgsql:IndexMethod", "gin")
                .Annotation("Npgsql:IndexOperators", new[] { "jsonb_ops" });

            migrationBuilder.CreateIndex(
                name: "IX_Student_Parents",
                table: "Student",
                column: "Parents")
                .Annotation("Npgsql:IndexMethod", "gin")
                .Annotation("Npgsql:IndexOperators", new[] { "jsonb_ops" });

            migrationBuilder.CreateIndex(
                name: "IX_User_Email",
                table: "User",
                column: "Email",
                unique: true)
                .Annotation("Npgsql:IndexOperators", new[] { "varchar_pattern_ops" });

            migrationBuilder.CreateIndex(
                name: "IX_User_MobileNumber",
                table: "User",
                column: "MobileNumber",
                unique: true)
                .Annotation("Npgsql:IndexOperators", new[] { "varchar_pattern_ops" });

            migrationBuilder.CreateIndex(
                name: "IX_User_Names",
                table: "User",
                column: "Names")
                .Annotation("Npgsql:IndexMethod", "gin")
                .Annotation("Npgsql:IndexOperators", new[] { "jsonb_ops" });

            migrationBuilder.CreateIndex(
                name: "IX_UserClaim_Name",
                table: "UserClaim",
                column: "Name")
                .Annotation("Npgsql:IndexOperators", new[] { "varchar_pattern_ops" });

            migrationBuilder.CreateIndex(
                name: "IX_UserClaim_UserId",
                table: "UserClaim",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Student");

            migrationBuilder.DropTable(
                name: "UserClaim");

            migrationBuilder.DropTable(
                name: "Bus");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Campus");

            migrationBuilder.DropTable(
                name: "School");
        }
    }
}
