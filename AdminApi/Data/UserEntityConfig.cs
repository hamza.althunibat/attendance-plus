using AttendancePlus.AdminApi.Data.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AttendancePlus.AdminApi.Data;

public class UserEntityConfig : IEntityTypeConfiguration<User> {
    public void Configure(EntityTypeBuilder<User> builder) {
        builder.UseXminAsConcurrencyToken();
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id).IsRequired().HasMaxLength(32);
        builder.Property(x => x.UserKey).IsRequired().HasMaxLength(32);
        builder.Property(x => x.Names).IsRequired().HasColumnType("jsonb");
        builder.HasIndex(x => x.Names).HasMethod("gin").HasOperators("jsonb_ops");
        builder.Property(x => x.MobileNumber).IsRequired(false).HasMaxLength(15);
        builder.Property(x => x.Email).IsRequired(false).HasMaxLength(256);
        builder.Property(x => x.MobileNumberConfirmed).IsRequired(false);
        builder.Property(x => x.EmailConfirmed).IsRequired(false);
        builder.Property(x => x.Role).IsRequired();
        builder.Property(x => x.CreatedOn).IsRequired();
        builder.Property(x => x.UpdatedOn).IsRequired(false);

        builder.HasIndex(x => x.Email).IsUnique().HasOperators("varchar_pattern_ops");
        builder.HasIndex(x => x.MobileNumber).IsUnique().HasOperators("varchar_pattern_ops");
    }
}