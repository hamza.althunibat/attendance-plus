using AttendancePlus.AdminApi.Data.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AttendancePlus.AdminApi.Data;

public class StudentEntityConfig : IEntityTypeConfiguration<Student> {
    public void Configure(EntityTypeBuilder<Student> builder) {
        builder.UseXminAsConcurrencyToken();
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id).IsRequired().HasMaxLength(32);
        builder.Property(x => x.CreatedOn).IsRequired();
        builder.Property(x => x.UpdatedOn).IsRequired(false);
        builder.Property(x => x.Names).IsRequired().HasColumnType("jsonb");
        builder.Property(x => x.Location).IsRequired().HasColumnType("geometry (point)");
        builder.Property(x => x.Parents).IsRequired().HasColumnType("jsonb");
        builder.HasIndex(x => x.Names).HasMethod("gin").HasOperators("jsonb_ops");
        builder.HasIndex(x => x.Parents).HasMethod("gin").HasOperators("jsonb_ops");
        builder.HasOne(x => x.Bus).WithMany(y => y.Students).HasForeignKey(x => x.BusId).IsRequired();
    }
}