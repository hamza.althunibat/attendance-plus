using AttendancePlus.AdminApi.Data.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AttendancePlus.AdminApi.Data;

public class UserClaimEntityConfig : IEntityTypeConfiguration<UserClaim> {
    public void Configure(EntityTypeBuilder<UserClaim> builder) {
        builder.UseXminAsConcurrencyToken();
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id).IsRequired().HasMaxLength(32);
        builder.Property(x => x.Name).IsRequired().HasMaxLength(255);
        builder.Property(x => x.Value).IsRequired().HasMaxLength(255);
        builder.Property(x => x.CreatedOn).IsRequired();
        builder.Property(x => x.UpdatedOn).IsRequired(false);
        builder.HasOne(x => x.User).WithMany(y => y.Claims).HasForeignKey(x => x.UserId).IsRequired();

        builder.HasIndex(x => x.Name).HasOperators("varchar_pattern_ops");
    }
}