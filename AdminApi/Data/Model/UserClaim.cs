using AttendancePlus.AdminApi.Data.Core.Model;

namespace AttendancePlus.AdminApi.Data.Model;

public class UserClaim : Entity<string> {
    public User User { get; set; }
    public string UserId { get; set; }

    public string Name { get; set; }
    public string Value { get; set; }
}