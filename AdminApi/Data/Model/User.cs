using AttendancePlus.AdminApi.Data.Core.Model;

namespace AttendancePlus.AdminApi.Data.Model;

public class User : Entity<string> {
    public User(string id) {
        Names = new HashSet<Name>();
        Id = id;
        Claims = new HashSet<UserClaim>();
    }

    public string MobileNumber { get; set; }
    public string Email { get; set; }
    public DateTimeOffset? MobileNumberConfirmed { get; set; }
    public DateTimeOffset? EmailConfirmed { get; set; }

    public ISet<Name> Names { get; init; }
    public string UserKey { get; set; }
    public Role Role { get; set; }
    public ISet<UserClaim> Claims { get; }
}