using AttendancePlus.AdminApi.Data.Core.Model;

namespace AttendancePlus.AdminApi.Data.Model;

public class School : Entity<string> {
    public School(string id) {
        Id = id;
        Names = new HashSet<Name>();
        Campuses = new HashSet<Campus>();
    }

    public ISet<Name> Names { get; }
    public ISet<Campus> Campuses { get; }
}