namespace AttendancePlus.AdminApi.Data.Model;

public readonly record struct UserAgent(string UserAgentString, string Name, string Type, string Version,
    string VersionMajor, DeviceModel DeviceModel, Engine Engine, OperatingSystem OperatingSystem);