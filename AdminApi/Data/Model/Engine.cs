namespace AttendancePlus.AdminApi.Data.Model;

public readonly record struct Engine(string Name, string Type, string Version, string VersionMajor, string Build);