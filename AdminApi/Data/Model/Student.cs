using AttendancePlus.AdminApi.Data.Core.Model;
using NetTopologySuite.Geometries;

namespace AttendancePlus.AdminApi.Data.Model;

public class Student : Entity<string> {
    public Student() {
        Names = new HashSet<Name>();
        Parents = new HashSet<ParentDetails>();
    }

    public Point Location { get; set; }
    public string BusId { get; set; }
    public Bus Bus { get; set; }
    public ISet<Name> Names { get; }
    public ISet<ParentDetails> Parents { get; }
}