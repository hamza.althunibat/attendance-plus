namespace AttendancePlus.AdminApi.Data.Model;

public enum Role {
    SchoolAdmin,
    CampusAdmin,
    HomeroomSupervisor,
    BusSupervisor,
    BusDevice,
    HomeroomDevice,
    Parent,
    User,
    SuperAdmin
}