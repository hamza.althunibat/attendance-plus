using AttendancePlus.AdminApi.Data.Core.Model;

namespace AttendancePlus.AdminApi.Data.Model;

public class Bus : Entity<string> {
    public Bus() {
        Names = new HashSet<Name>();
        Students = new HashSet<Student>();
    }

    public string CampusId { get; set; }
    public Campus Campus { get; set; }
    public ISet<Name> Names { get; }
    public ISet<Student> Students { get; }
}