using AttendancePlus.AdminApi.Data.Core.Model;

namespace AttendancePlus.AdminApi.Data.Model;

public class DeviceLogin : Entity<long> {
    public string DeviceId { get; set; }
    public Device Device { get; set; }
    public GeoLocation Location { get; set; }
}