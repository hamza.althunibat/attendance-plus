namespace AttendancePlus.AdminApi.Data.Model;

public readonly record struct StudentInfo(string Id, string SchoolId);