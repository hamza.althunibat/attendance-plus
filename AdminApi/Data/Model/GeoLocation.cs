using NetTopologySuite.Geometries;

namespace AttendancePlus.AdminApi.Data.Model;

public readonly record struct GeoLocation(string Ip, Country Country, string City, Point Coordinates);