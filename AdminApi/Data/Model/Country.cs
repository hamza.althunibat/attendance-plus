namespace AttendancePlus.AdminApi.Data.Model;

public readonly record struct Country(string Code, string Name);