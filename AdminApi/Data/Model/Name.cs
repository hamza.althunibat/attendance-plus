namespace AttendancePlus.AdminApi.Data.Model;

public readonly record struct Name(string Language, string Text);