using AttendancePlus.AdminApi.Data.Core.Model;

namespace AttendancePlus.AdminApi.Data.Model;

public class Device : Entity<string> {
    public Device(string id) {
        Id = id;
        Logins = new HashSet<DeviceLogin>();
    }

    public string Auth { get; set; }

    public DateTimeOffset? TokenConfirmed { get; set; }
    public string Endpoint { get; set; }
    public string P256dh { get; set; }
    public UserAgent? UserAgent { get; set; }
    public string UserId { get; set; }
    public User User { get; set; }
    public ICollection<DeviceLogin> Logins { get; set; }
}