namespace AttendancePlus.AdminApi.Data.Model;

public readonly record struct OperatingSystem(string Name, string Type, string Version, string VersionMajor);