namespace AttendancePlus.AdminApi.Data.Model;

public enum Relation {
    Father,
    Mother,
    Others
}