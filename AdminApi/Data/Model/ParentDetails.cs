using NetTopologySuite.Geometries;

namespace AttendancePlus.AdminApi.Data.Model;

public readonly record struct ParentDetails(ISet<Name> Names, Relation Relation, string MobileNumber, Point Location,
    bool IsDefault);