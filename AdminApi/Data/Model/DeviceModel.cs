namespace AttendancePlus.AdminApi.Data.Model;

public readonly record struct DeviceModel(string Name, string Type, string Brand, string Cpu);