using AttendancePlus.AdminApi.Data.Core.Model;
using NetTopologySuite.Geometries;

namespace AttendancePlus.AdminApi.Data.Model;

public class Campus : Entity<string> {
    public Campus() {
        Buses = new HashSet<Bus>();
        Names = new HashSet<Name>();
    }

    public ISet<Name> Names { get; }
    public string SchoolId { get; set; }
    public School School { get; set; }
    public Point Location { get; set; }
    public ISet<Bus> Buses { get; }
}