using AttendancePlus.AdminApi.Data.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AttendancePlus.AdminApi.Data;

public class BusEntityConfig : IEntityTypeConfiguration<Bus> {
    public void Configure(EntityTypeBuilder<Bus> builder) {
        builder.UseXminAsConcurrencyToken();
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id).IsRequired().HasMaxLength(32);
        builder.Property(x => x.CreatedOn).IsRequired();
        builder.Property(x => x.UpdatedOn).IsRequired(false);
        builder.Property(x => x.Names).IsRequired().HasColumnType("jsonb");
        builder.HasIndex(x => x.Names).HasMethod("gin").HasOperators("jsonb_ops");

        builder.HasOne(x => x.Campus).WithMany(y => y.Buses).HasForeignKey(x => x.CampusId).IsRequired();
    }
}