using System.Data.Common;
using AttendancePlus.AdminApi.Data.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace AttendancePlus.AdminApi.Data.Core;

public class EfUnitOfWork<TContext> : IUnitOfWork<TContext> where TContext : DbContext {
    private readonly TContext _context;
    private readonly ILogger _logger;

    public EfUnitOfWork(ILoggerFactory loggerFactory, TContext context) {
        _context = context;
        _logger = loggerFactory.CreateLogger<EfUnitOfWork<TContext>>();
    }

    public async Task<EntityResult> SaveAsync(CancellationToken cancellationToken = default) {
        try {
            _logger.LogInformation("Attempt to Save Data");
            await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            _logger.LogInformation("Data has been saved!");
            return EntityResult.Success;
        }
        catch (DbUpdateConcurrencyException ex) {
            _logger.LogError(ex,
                "Unable to save data changes due to concurrency check violation");
            return EntityResult.Failed(new Error("Concurrency Exception", ex.Message));
        }
        catch (DbUpdateException ex) when (ex.InnerException is DbException err) {
            _logger.LogError(err, "Unable to save data changes");
            return EntityResult.Failed(new Error("Db Update Exception", err.Message));
        }
        catch (DbUpdateException ex) {
            _logger.LogError(ex, "Unable to save data changes");
            return EntityResult.Failed(new Error("Db Update Exception", ex.Message));
        }
    }

    public async Task<EntityResult> SaveAsync(DbTransaction transaction,
        CancellationToken cancellationToken = default) {
        if (transaction == null) {
            _logger.LogError("A transaction is required!");
            return EntityResult.Failed(new Error("A transaction is required!",
                "argument null exception"));
        }

        try {
            _logger.LogInformation("Attempt to Save Data");
            await _context.Database.UseTransactionAsync(transaction, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            _logger.LogInformation("Data has been saved!");
            return EntityResult.Success;
        }
        catch (DbUpdateConcurrencyException ex) {
            _logger.LogError(ex,
                "Unable to save data changes due to concurrency check violation");
            return EntityResult.Failed(new Error("Concurrency Exception", ex.Message));
        }
        catch (DbUpdateException ex) when (ex.InnerException is DbException err) {
            _logger.LogError(ex, "Unable to save data changes");
            return EntityResult.Failed(new Error("Db Update Exception", err.Message));
        }
        catch (DbUpdateException ex) {
            _logger.LogError(ex, "Unable to save data changes");
            return EntityResult.Failed(new Error("Db Update Exception", ex.Message));
        }
    }
}