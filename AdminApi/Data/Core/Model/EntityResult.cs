namespace AttendancePlus.AdminApi.Data.Core.Model;

public class EntityResult {
    private EntityResult(Error error) {
        Error = error;
        Succeeded = false;
    }

    private EntityResult() {
        Succeeded = true;
    }

    /// <summary>
    ///     Flag indicating whether if the operation succeeded or not.
    /// </summary>
    /// <value>True if the operation succeeded, otherwise false.</value>
    public bool Succeeded { get; }

    public Error? Error { get; }

    public static EntityResult Success { get; } = new();

    public static EntityResult Failed(Error error) {
        return new EntityResult(error);
    }
}