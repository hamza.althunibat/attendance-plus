namespace AttendancePlus.AdminApi.Data.Core.Model;

public interface IEntity<out TId> where TId : IEquatable<TId> {
    TId Id { get; }
}