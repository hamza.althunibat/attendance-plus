using NodaTime;

namespace AttendancePlus.AdminApi.Data.Core.Model;

public abstract class Entity<TId> : IEntity<TId> where TId : IEquatable<TId> {
    protected Entity() { }

    protected Entity(TId id) {
        Id = id ?? throw new ArgumentNullException(nameof(id));
    }

    public ZonedDateTime CreatedOn { get; set; }
    public ZonedDateTime? UpdatedOn { get; set; }

    public TId Id { get; protected set; }

    public bool Equals(Entity<TId> other) {
        return EqualityComparer<TId>.Default.Equals(Id, other.Id);
    }

    public override bool Equals(object obj) {
        if (obj is null) return false;
        if (ReferenceEquals(this, obj)) return true;
        return obj.GetType() == GetType() && Equals((Entity<TId>)obj);
    }

    public override int GetHashCode() {
        // ReSharper disable once NonReadonlyMemberInGetHashCode
        return HashCode.Combine(Id);
    }

    public static bool operator ==(Entity<TId> left, Entity<TId> right) {
        return Equals(left, right);
    }

    public static bool operator !=(Entity<TId> left, Entity<TId> right) {
        return !Equals(left, right);
    }
}