namespace AttendancePlus.AdminApi.Data.Core.Model;

public readonly record struct Error(string Message, string Exception);