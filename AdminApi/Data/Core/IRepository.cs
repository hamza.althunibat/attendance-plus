using System.Linq.Expressions;
using AttendancePlus.AdminApi.Data.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace AttendancePlus.AdminApi.Data.Core;

public interface IRepository<TEntity, TContext, TId> : IReadOnlyRepository<TEntity, TContext, TId>
    where TId : IEquatable<TId>
    where TContext : DbContext
    where TEntity : class, IEntity<TId> {
    Task Add(TEntity item, CancellationToken cancellationToken = default);
    Task<bool> Edit(TEntity item, CancellationToken cancellationToken = default);
    Task<bool> Delete(TId id, CancellationToken cancellationToken = default);

    Task<bool> DeleteRange(Expression<Func<TEntity, bool>> filter,
        CancellationToken cancellationToken = default);

    Task AddRange(IEnumerable<TEntity> items, CancellationToken cancellationToken = default);
}