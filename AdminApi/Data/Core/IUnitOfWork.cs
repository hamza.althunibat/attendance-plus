using System.Data.Common;
using AttendancePlus.AdminApi.Data.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace AttendancePlus.AdminApi.Data.Core;

public interface IUnitOfWork<TContext> where TContext : DbContext {
    Task<EntityResult> SaveAsync(CancellationToken cancellationToken = default);

    Task<EntityResult> SaveAsync(DbTransaction transaction,
        CancellationToken cancellationToken = default);
}