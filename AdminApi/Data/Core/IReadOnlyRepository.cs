using System.Data.Common;
using System.Linq.Expressions;
using AttendancePlus.AdminApi.Data.Core.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace AttendancePlus.AdminApi.Data.Core;

public interface IReadOnlyRepository<TEntity, TContext, TId>
    where TEntity : class, IEntity<TId>
    where TContext : DbContext
    where TId : IEquatable<TId> {
    Task<bool> Any(Expression<Func<TEntity, bool>> filter,
        CancellationToken cancellationToken = default);

    Task<PaginatedList<TResult>> GetPagedItems<TResult>(
        Expression<Func<TEntity, bool>> filter,
        Expression<Func<TEntity, TResult>> selector,
        int pageIndex = 1,
        int pageSize = 10,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
        bool disableTracking = true,
        CancellationToken cancellationToken = default
    ) where TResult : class;

    IAsyncEnumerable<TResult> GetBy<TResult>(
        Expression<Func<TEntity, bool>> filter,
        Expression<Func<TEntity, TResult>> selector,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
        bool disableTracking = true,
        CancellationToken cancellationToken = default
    );
    
    IAsyncEnumerable<TEntity> GetBy(
        string rowSql,
        DbParameter[] parameters,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
        bool disableTracking = true,
        CancellationToken cancellationToken = default
    );

    IAsyncEnumerable<TResult> GetAll<TResult>(Expression<Func<TEntity, TResult>> selector,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
        bool disableTracking = true,
        CancellationToken cancellationToken = default
    );

    Task<TResult> GetFirstOrDefaultBy<TResult>(Expression<Func<TEntity, bool>> filter,
        Expression<Func<TEntity, TResult>> selector,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
        bool disableTracking = true,
        CancellationToken cancellationToken = default
    );

    Task<bool> All(Expression<Func<TEntity, bool>> filter,
        CancellationToken cancellationToken = default);
}