using System.Linq.Expressions;
using AttendancePlus.AdminApi.Data.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace AttendancePlus.AdminApi.Data.Core;

internal static class Extensions {
    internal static async Task<PaginatedList<TResult>>
        CreatePaginatedListAsync<TEntity, TId, TResult>(this IQueryable<TEntity> source,
            Expression<Func<TEntity, TResult>>
                selector, int pageIndex,
            int pageSize,
            CancellationToken cancellationToken =
                default)
        where TEntity : IEntity<TId> where TId : IEquatable<TId> where TResult : class {
        var count = await source.CountAsync(cancellationToken).ConfigureAwait(false);
        return new
            PaginatedList<TResult>(
                source.Skip((pageIndex - 1) * pageSize).Take(pageSize).Select(selector),
                count, pageIndex, pageSize);
    }

    internal static PaginatedList<TResult> CreatePaginatedList<TEntity, TId, TResult>(
        this IQueryable<TEntity> source, Expression<Func<TEntity, TResult>> selector,
        int pageIndex, int pageSize) where TEntity : IEntity<TId>
        where TId : IEquatable<TId>
        where TResult : class {
        var totalCount = source.Count();
        return new
            PaginatedList<TResult>(
                source.Skip((pageIndex - 1) * pageSize).Take(pageSize).Select(selector),
                totalCount, pageIndex, pageSize);
    }
}