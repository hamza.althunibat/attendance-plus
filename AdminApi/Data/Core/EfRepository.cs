using System.Data.Common;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using AttendancePlus.AdminApi.Data.Core.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace AttendancePlus.AdminApi.Data.Core;

public class EfRepository<TEntity, TContext, TId> : IRepository<TEntity, TContext, TId>
    where TEntity : class, IEntity<TId>
    where TContext : DbContext
    where TId : IEquatable<TId> {
    private readonly DbSet<TEntity> _entities;

    public EfRepository(TContext dbContext) {
        _entities = dbContext.Set<TEntity>();
    }


    public async Task<bool> Any(
        Expression<Func<TEntity, bool>> filter,
        CancellationToken cancellationToken = default) {
        cancellationToken.ThrowIfCancellationRequested();
        return await _entities.AsNoTracking().AnyAsync(filter, cancellationToken);
    }

    public Task<PaginatedList<TResult>> GetPagedItems<TResult>(
        Expression<Func<TEntity, bool>> filter,
        Expression<Func<TEntity, TResult>> selector, int pageNumber = 1, int pageSize = 10,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
        bool disableTracking = true,
        CancellationToken cancellationToken = default
    ) where TResult : class {
        cancellationToken.ThrowIfCancellationRequested();
        IQueryable<TEntity> queryable = _entities;
        queryable = queryable.Where(filter);
        if (disableTracking) queryable = queryable.AsNoTracking();
        if (include != null) queryable = include(queryable);
        if (orderBy != null) queryable = orderBy(queryable);
        return queryable.CreatePaginatedListAsync<TEntity, TId, TResult>(selector, pageNumber, pageSize,
            cancellationToken);
    }

    public async IAsyncEnumerable<TResult> GetBy<TResult>(
        Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TResult>> selector,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
        bool disableTracking = true,
        [EnumeratorCancellation] CancellationToken cancellationToken = default) {
        cancellationToken.ThrowIfCancellationRequested();
        IQueryable<TEntity> queryable = _entities;
        queryable = queryable.Where(filter);
        if (disableTracking) queryable = queryable.AsNoTracking();
        if (include != null) queryable = include(queryable);
        await foreach (var item in queryable.Select(selector).AsAsyncEnumerable().WithCancellation(cancellationToken)
                           .ConfigureAwait(false))
            yield return item;
    }

    public async IAsyncEnumerable<TEntity> GetBy(string rowSql, DbParameter[] parameters,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null, bool disableTracking = true,
        [EnumeratorCancellation] CancellationToken cancellationToken = default) {
        var queryable = _entities.FromSqlRaw(rowSql, parameters);
        if (disableTracking) queryable = queryable.AsNoTracking();
        if (include != null) queryable = include(queryable);
        await foreach (var item in queryable.AsAsyncEnumerable().WithCancellation(cancellationToken)
                           .ConfigureAwait(false))
            yield return item;
    }

    public Task<TResult> GetFirstOrDefaultBy<TResult>(Expression<Func<TEntity, bool>> filter,
        Expression<Func<TEntity, TResult>> selector,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
        bool disableTracking = true,
        CancellationToken cancellationToken = default) {
        cancellationToken.ThrowIfCancellationRequested();
        IQueryable<TEntity> queryable = _entities;
        queryable = queryable.Where(filter);
        if (disableTracking) queryable = queryable.AsNoTracking();
        if (include != null) queryable = include(queryable);
        return queryable
            .Select(selector)
            .FirstOrDefaultAsync(cancellationToken);
    }

    public async IAsyncEnumerable<TResult> GetAll<TResult>(
        Expression<Func<TEntity, TResult>> selector,
        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
        bool disableTracking = true,
        [EnumeratorCancellation] CancellationToken cancellationToken = default) {
        cancellationToken.ThrowIfCancellationRequested();
        IQueryable<TEntity> queryable = _entities;
        if (disableTracking) queryable = queryable.AsNoTracking();
        if (include != null) queryable = include(queryable);
        await foreach (var item in queryable.Select(selector).AsAsyncEnumerable().WithCancellation(cancellationToken)
                           .ConfigureAwait(false))
            yield return item;
    }

    public Task<bool> All(Expression<Func<TEntity, bool>> filter,
        CancellationToken cancellationToken = default) {
        cancellationToken.ThrowIfCancellationRequested();
        return _entities.AsNoTracking().AllAsync(filter, cancellationToken);
    }

    public Task Add(TEntity item, CancellationToken cancellationToken = default) {
        cancellationToken.ThrowIfCancellationRequested();
        if (item == null)
            throw new ArgumentNullException(nameof(item));
        _entities.Add(item);
        return Task.CompletedTask;
    }

    public Task<bool> Edit(TEntity item, CancellationToken cancellationToken = default) {
        cancellationToken.ThrowIfCancellationRequested();
        if (item == null)
            throw new ArgumentNullException(nameof(item));
        _entities.Attach(item);
        _entities.Update(item);
        return Task.FromResult(true);
    }

    public async Task<bool> Delete(TId id, CancellationToken cancellationToken = default) {
        cancellationToken.ThrowIfCancellationRequested();
        if (id == null)
            throw new ArgumentNullException(nameof(id));
        var item = await GetFirstOrDefaultBy(x => id.Equals(x.Id), x => x, disableTracking: false,
                cancellationToken: cancellationToken)
            .ConfigureAwait(false);
        if (item == null) return false;
        _entities.Remove(item);
        return true;
    }

    public Task<bool> DeleteRange(Expression<Func<TEntity, bool>> filter,
        CancellationToken cancellationToken = default) {
        cancellationToken.ThrowIfCancellationRequested();
        var items = _entities.AsNoTracking().Where(filter);
        if (items == null)
            throw new ArgumentNullException(nameof(items));
        _entities.RemoveRange(items);
        return Task.FromResult(true);
    }

    public Task AddRange(IEnumerable<TEntity> items,
        CancellationToken cancellationToken = default) {
        cancellationToken.ThrowIfCancellationRequested();
        if (items == null)
            throw new ArgumentNullException(nameof(items));
        return _entities.AddRangeAsync(items, cancellationToken);
    }
}