using AttendancePlus.AdminApi.Data.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Npgsql;

namespace AttendancePlus.AdminApi.Data;

public class AdministrationDbContext : DbContext {
    static AdministrationDbContext() {
        NpgsqlConnection.GlobalTypeMapper
            .MapEnum<Relation>()
            .MapEnum<Role>();
    }

    public AdministrationDbContext(DbContextOptions options) : base(options) { }

    protected override void OnModelCreating(ModelBuilder builder) {
        builder.HasPostgresEnum<Relation>();
        builder.HasPostgresEnum<Role>();
        builder.HasPostgresExtension("postgis");

        builder.ApplyConfiguration(new BusEntityConfig());
        builder.ApplyConfiguration(new CampusEntityConfig());
        builder.ApplyConfiguration(new SchoolEntityConfig());
        builder.ApplyConfiguration(new StudentEntityConfig());
        builder.ApplyConfiguration(new UserEntityConfig());
        builder.ApplyConfiguration(new UserClaimEntityConfig());
    }
}

public class AdministrationDbContextFactory : IDesignTimeDbContextFactory<AdministrationDbContext> {
    public AdministrationDbContext CreateDbContext(string[] args) {
        var optionsBuilder = new DbContextOptionsBuilder<AdministrationDbContext>();
        optionsBuilder.UseNpgsql("Data Source=blog.db", cfg => {
            cfg.UseAdminDatabase("postgres");
            cfg.MigrationsAssembly(typeof(AdministrationDbContext).Assembly.FullName);
            cfg.UseNetTopologySuite();
            cfg.UseNodaTime();
        });

        return new AdministrationDbContext(optionsBuilder.Options);
    }
}