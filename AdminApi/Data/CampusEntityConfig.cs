using AttendancePlus.AdminApi.Data.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AttendancePlus.AdminApi.Data;

public class CampusEntityConfig : IEntityTypeConfiguration<Campus> {
    public void Configure(EntityTypeBuilder<Campus> builder) {
        builder.UseXminAsConcurrencyToken();
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id).IsRequired().HasMaxLength(32);
        builder.Property(x => x.CreatedOn).IsRequired();
        builder.Property(x => x.UpdatedOn).IsRequired(false);
        builder.Property(x => x.Location).IsRequired().HasColumnType("geometry (point)");

        builder.Property(x => x.Names).IsRequired().HasColumnType("jsonb");
        builder.HasIndex(x => x.Names).HasMethod("gin").HasOperators("jsonb_ops");

        builder.HasOne(x => x.School).WithMany(y => y.Campuses).HasForeignKey(x => x.SchoolId).IsRequired();
    }
}